new Swiper('.main__slider', {
    autoHeight: true,
    slidesPerView: 2.2
})

new Swiper('.plant__slider', {
    autoHeight: true,
    slidesPerView: 1.2,
    spaceBetween: 20,
})

const burger = document.querySelector('.header__burger')
const headerMenu = document.querySelector('.header__menu')

burger.addEventListener('click', () => {
    headerMenu.classList.toggle('active')
})